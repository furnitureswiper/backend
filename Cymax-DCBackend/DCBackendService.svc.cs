﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceModel;
using Cymax_DCBackend.Models;
using Helpers;

namespace Cymax_DCBackend
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class DCBackendService : IDCBackendService
    {
        static string ConnectionString = ConfigurationManager.ConnectionStrings["CymaxConnection"].ConnectionString;
        static int ClientId = int.Parse(ConfigurationManager.AppSettings["ClientID"]);
        string emailPassword;

        [AuthorizedMethodAttribute]
        public JsonObject RegisterUser(string email, string turk, string password)
        {
            string queryString = @"INSERT INTO [Users]  
                                               (ClientID, Email,TurkAccount,Password,JoinDate) 
                                        SELECT @ClientID, @email, @turk, @password, @joindate
                                         WHERE NOT EXISTS (SELECT 1 FROM Users WHERE ClientID = @ClientID AND LOWER(Email) = LOWER(@email))";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.Parameters.AddWithValue("@ClientID", ClientId);
                command.Parameters.AddWithValue("@email", email);
                command.Parameters.AddWithValue("@turk", turk);
                command.Parameters.AddWithValue("@password", password);
                command.Parameters.AddWithValue("@joindate", DateTime.Now);
                connection.Open();
                int rowcount = command.ExecuteNonQuery();
                if (rowcount == 1)
                {
                    return new JsonObject() { result = "successful" };
                }
                else
                {
                    return new JsonObject() { result = "failed" };
                }
            }
        }

        [AuthorizedMethodAttribute]
        public JsonObject Login(string email, string password)
        {
            string acct = string.Empty, turkacct = string.Empty, userId = string.Empty;

            string queryString = "SELECT Email, TurkAccount, UserID from users WHERE email=@email AND password=@password AND ClientID = @ClientID";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.Parameters.AddWithValue("@email", email);
                command.Parameters.AddWithValue("@password", password);
                command.Parameters.AddWithValue("@ClientID", ClientId);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    acct = reader.GetString(0);
                    turkacct = reader.GetString(1);
                    userId = reader.GetInt32(2).ToString();
                }
                return new JsonObject() { result = String.Format("{0},{1},{2}", acct, turkacct, userId) };
            }
        }

        [AuthorizedMethodAttribute]
        // Backend function to email temporary password
        public string ResetPasswordEmail(string email)
        {
            emailPassword = RandomPassword();

            string queryString = "UPDATE users SET password=@password WHERE email=@email AND ClientID = @ClientID";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.Parameters.AddWithValue("@email", email);
                command.Parameters.AddWithValue("@password", Helpers.Encryption.Encrypt(emailPassword));
                command.Parameters.AddWithValue("@ClientID", ClientId);
                connection.Open();
                int rowcount = command.ExecuteNonQuery();
                if (rowcount > 0)
                {
                    SendEmail(email, emailPassword);
                    return "successful";
                }
                else
                {
                    return "failed";
                }
            }
        }

        // Backend function to send the email to user
        private void SendEmail(string email, string emailPass)
        {
            MailMessage message = new System.Net.Mail.MailMessage();
            string fromEmail = "cymax.data@gmail.com";
            string fromPW = "Cymax2016";
            string toEmail = email;
            message.From = new MailAddress(fromEmail);
            message.To.Add(toEmail);
            message.Subject = "Cymax Data Cleansing Password Recovery";
            message.Body = "Hi! \n\r\n\rThis is your new  password: " + emailPass + "\n\rYou can change this password in your account settings using the app. \n\r\n\rThank you.";
            message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            using (SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587))
            {
                smtpClient.EnableSsl = true;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(fromEmail, fromPW);

                smtpClient.Send(message.From.ToString(), message.To.ToString(), message.Subject, message.Body);
            }
        }

        // Backend function to generate a temporary password
        private string RandomPassword()
        {
            string path = Path.GetRandomFileName().Replace(".", "").Substring(0,6);
            return path;
        }

        // Backend function to change the temporary password to new password user assigned
        public string ResetPassword(string email, string tempPassword, string newPassword)
        {
            string queryString = "UPDATE users SET password=@newPass WHERE email=@email AND password=@temp AND ClientID = @ClientID";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.Parameters.AddWithValue("@email", email);
                command.Parameters.AddWithValue("@temp", tempPassword);
                command.Parameters.AddWithValue("@newPass", newPassword);
                command.Parameters.AddWithValue("@ClientID", ClientId);
                connection.Open();
                int rowcount = command.ExecuteNonQuery();
                if (rowcount == 1)
                {
                    return "successful";
                }
                else
                {
                    return "failed";
                }
            }
        }

        public List<String> Category()
        {
            string queryString = "SELECT DISTINCT category from [Attributes] WHERE ClientID = @ClientID";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.Parameters.AddWithValue("@ClientID", ClientId);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                List<String> result = new List<string>();
                while (reader.Read())
                {
                    result.Add(reader.GetString(0));
                }
                return result;
            }
        }

        public List<ProductData> GetSwipeCards(string parentCategory)
        {
            List<ProductData> result = new List<ProductData>();
            List<ProductData> tempProdData = new List<ProductData>();

            try
            {
                string queryString = @"  SELECT TOP 3 ProductID, Name, P.Category, Description, ImageUrl
                                           FROM [Products] P
									CROSS APPLY (
													SELECT  TOP 1 ParentCategory
													FROM    [Attributes] A
													WHERE   A.Category = P.Category
												) as A
										  WHERE UPPER(@ParentCategory) IN ('','ALL', A.ParentCategory)
                                            AND P.ClientID = @ClientID
                                       ORDER BY NEWID()";

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(queryString, connection))
                    {
                        command.Parameters.AddWithValue("@ParentCategory", parentCategory);
                        command.Parameters.AddWithValue("@ClientID", ClientId);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.HasRows)
                        {
                            int limiter = 0;
                            while (reader.Read() && limiter++ < 3)
                            {
                                tempProdData.Add(new ProductData
                                {
                                    Id = reader.GetInt32(0),
                                    Name = reader.GetString(1),
                                    Category = reader.GetString(2),
                                    Description = reader.GetString(3),
                                    ImageUrl = reader.GetString(4)
                                });
                            }
                        }
                        else
                        {
                            Console.WriteLine("No products found.");
                        }
                        
                    }

                    foreach (ProductData prodData in tempProdData)
                    {

                        queryString = @"SELECT  [Category]
                                               ,[AttributeGroup]
                                               ,[Attribute]
                                               ,[ParentCategory] 
                                          FROM  [Attributes]
                                         WHERE  Category = @Category
                                           AND  UPPER(@ParentCategory) IN ('','ALL', ParentCategory)
                                           AND  ClientID = @ClientID
                                      ORDER BY  AttributeGroup";

                        connection.Close();
                        using (SqlCommand command = new SqlCommand(queryString, connection))
                        {
                            command.Parameters.AddWithValue("@Category", prodData.Category);
                            command.Parameters.AddWithValue("@ParentCategory", parentCategory);
                            command.Parameters.AddWithValue("@ClientID", ClientId);
                            connection.Open();
                            SqlDataReader reader = command.ExecuteReader();

                            string lastAttrGroup = String.Empty;
                            AttributeData attr;
                            prodData.AttributeDataList = new List<AttributeData>();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    attr = new AttributeData
                                    {
                                        Category = reader.GetString(0),
                                        AttributeGroup = reader.GetString(1),
                                        Attribute = reader.GetString(2),
                                        ParentCategory = reader.GetString(3)

                                    };

                                    if (lastAttrGroup != attr.AttributeGroup) {
                                        result.Add(new ProductData
                                        {
                                            Id = prodData.Id,
                                            Name = prodData.Name,
                                            Category = attr.Category,
                                            Description = prodData.Description,
                                            ImageUrl = prodData.ImageUrl,
                                            AttributeGroup = attr.AttributeGroup,
                                            Attribute = attr.AttributeGroup

                                        });
                                        result.Last().AttributeDataList = new List<AttributeData>();
                                    }

                                    result.Last().AttributeDataList.Add(attr);
                                    lastAttrGroup = attr.AttributeGroup;
                                     
                                }
                            }
                            else
                            {
                                Console.WriteLine("No product attributes found.");
                            }

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return result;
        }

        [AuthorizedMethodAttribute]
        public JsonObject SaveProductReview(ProductReviewData reviewData, List<AttributeData> savedAttributes)
        {
            try
            {
                string queryString = @"INSERT INTO [ProductsReview]
                                                   ([ClientID]
                                                   ,[UserID]
                                                   ,[Date]
                                                   ,[MacAddress]
                                                   ,[ProductID]
                                                   ,[AttributeGroup]
                                                   ,[Attribute]
                                                   ,[Added])
                                            VALUES (@ClientID
                                                   ,@UserID
                                                   ,@Date
                                                   ,@MacAddress
                                                   ,@ProductID
                                                   ,@AttributeGroup
                                                   ,@Attribute
                                                   ,@Added)";

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = new SqlCommand(queryString, connection))
                {
                    connection.Open();

                    foreach (var item in savedAttributes)
                    {
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@ClientID", ClientId);
                        command.Parameters.AddWithValue("@UserID", reviewData.UserId);
                        command.Parameters.AddWithValue("@Date", DateTime.Now);
                        command.Parameters.AddWithValue("@MacAddress", reviewData.MacAddress);
                        command.Parameters.AddWithValue("@ProductID", reviewData.ProductId);
                        command.Parameters.AddWithValue("@AttributeGroup", item.AttributeGroup);
                        command.Parameters.AddWithValue("@Attribute", item.Attribute);
                        command.Parameters.AddWithValue("@Added", item.AttributeAdded);
                        command.ExecuteNonQuery();
                    }

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return new JsonObject() { result = "0" };
        }

        [AuthorizedMethodAttribute]
        public string AccountUpdate(string oldemail, string email, string turk)
        {
            string queryString = "UPDATE users SET email=@email, turkaccount=@turk WHERE email=@currentemail AND ClientID = @ClientID";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.Parameters.AddWithValue("@email", email);
                command.Parameters.AddWithValue("@turk", turk);
                command.Parameters.AddWithValue("@currentemail", oldemail);
                command.Parameters.AddWithValue("@ClientID", ClientId);
                connection.Open();
                int rowcount = command.ExecuteNonQuery();
                if (rowcount == 1)
                {
                    return "successful";
                }
                else
                {
                    return "failed";
                }
            }
        }
        [AuthorizedMethodAttribute]
        public JsonObject PasswordUpdate(string email, string oldpw, string newpw)
        {
            string queryString = "UPDATE users SET password=@newpw WHERE email=@email AND password=@oldpw AND ClientID = @ClientID";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.Parameters.AddWithValue("@email", email);
                command.Parameters.AddWithValue("@oldpw", oldpw);
                command.Parameters.AddWithValue("@newpw", newpw);
                command.Parameters.AddWithValue("@ClientID", ClientId);
                connection.Open();
                int rowcount = command.ExecuteNonQuery();
                
                if (rowcount == 1)
                {
                    return new JsonObject() { result = "successful" };
                }
                else
                {
                    return new JsonObject() { result = "failed" };
                }
            }
        }

        public string TestEnc(string passphrase)
        {
            return Helpers.Encryption.Encrypt(passphrase);
        }
    }
}
