﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Cymax_DCBackend.Startup))]
namespace Cymax_DCBackend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
