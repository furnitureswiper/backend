﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Cymax_DCBackend
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "LoginRoute",
                "Login",
                new { controller = "User", action = "Login" }
            );

            routes.MapRoute(
                "LogouRoute",
                "Logout",
                new { controller = "User", action = "Logout" }
            );

            routes.MapRoute(
                "RegisterRoute",
                "Register",
                new { controller = "User", action = "Register" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{action}/{id}",
                defaults: new { controller = "Backend", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
