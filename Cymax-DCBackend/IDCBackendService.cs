﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.IO;
using Cymax_DCBackend.Models;

namespace Cymax_DCBackend
{

    [ServiceContract(Namespace = "http://services.cymax.com")]
    public interface IDCBackendService
    {
        [OperationContract]
        [WebInvoke(
                    Method = "POST",
                    UriTemplate = "/RegisterUser",
                    BodyStyle = WebMessageBodyStyle.WrappedRequest,
                    ResponseFormat = WebMessageFormat.Json,
                    RequestFormat = WebMessageFormat.Json)]
        JsonObject RegisterUser(string email, string turk, string password);

        [OperationContract]
        [WebInvoke(
                    Method = "POST",
                    UriTemplate = "/Login",
                    BodyStyle = WebMessageBodyStyle.WrappedRequest,
                    ResponseFormat = WebMessageFormat.Json,
                    RequestFormat = WebMessageFormat.Json)]

        JsonObject Login(string email, string password);

        [OperationContract]
        [WebGet(UriTemplate = "/ResetPasswordEmail/{email}")]
        string ResetPasswordEmail(string email);

        [OperationContract]
        [WebGet(UriTemplate = "/ResetPassword/{email}/{tempPass}/{newPass}")]
        string ResetPassword(string email, string tempPass, string newPass);

        [OperationContract]
        [WebGet(UriTemplate = "/Category")]
        List<string> Category();
   

        [OperationContract]
        [WebGet( UriTemplate = "/GetSwipeCards/{category}",
                 BodyStyle = WebMessageBodyStyle.WrappedRequest,
                 ResponseFormat = WebMessageFormat.Json,
                 RequestFormat = WebMessageFormat.Json)]
        List<ProductData> GetSwipeCards(string category);

        [OperationContract]
        [WebInvoke(
                    Method = "POST",
                    UriTemplate = "/SaveProductReview",
                    BodyStyle = WebMessageBodyStyle.WrappedRequest,
                    ResponseFormat = WebMessageFormat.Json,
                    RequestFormat = WebMessageFormat.Json)]
        JsonObject SaveProductReview(ProductReviewData reviewData, List<AttributeData> savedAttributes);


        [OperationContract]
        [WebGet(UriTemplate = "/AccountUpdate/{oldemail}/{email}/{turk}")]
        string AccountUpdate(string oldemail, string email, string turk);

        [OperationContract]
        [WebInvoke(
                    Method = "POST",
                    UriTemplate = "/PasswordUpdate",
                    BodyStyle = WebMessageBodyStyle.WrappedRequest,
                    ResponseFormat = WebMessageFormat.Json,
                    RequestFormat = WebMessageFormat.Json)]
        JsonObject PasswordUpdate(string email, string oldpw, string newpw);
        
        [OperationContract]
        [WebGet(UriTemplate = "/TestEnc/{passphrase}")]
        string TestEnc(string passphrase);

    }
}
