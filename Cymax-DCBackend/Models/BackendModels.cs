﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Cymax_DCBackend.Models
{
    public class BackendModels
    {
        static string ConnectionString = ConfigurationManager.ConnectionStrings["CymaxConnection"].ConnectionString;

        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        // <summary>
        // Checks if user with given password exists in the database
        // </summary>
        // <param name="_username">User name</param>
        // <param name="_password">User password</param>
        // <returns>True if user exist and password is correct</returns>
        public bool IsValid(string _username, string _password)
        {
            string queryString = "SELECT [username] FROM [dbo].[BackendUsers] WHERE [username] = @u AND [password] = @p";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.Parameters.AddWithValue("@u", _username);
                command.Parameters.AddWithValue("@p", Helpers.Encryption.Encrypt(_password));

                connection.Open();

                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Dispose();
                    command.Dispose();
                    return true;
                }
                else
                {
                    reader.Dispose();
                    command.Dispose();
                    return false;
                }
            }
        }
    }
}