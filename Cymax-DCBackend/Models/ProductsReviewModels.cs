﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cymax_DCBackend.Models
{
    public class ProductsReviewModels
    {
        [Display(Name = "Product ID")]
        public string productID { get; set; }

        [Required]
        [Display(Name = "From")]
        public DateTime From { get; set; }

        [Required]
        [Display(Name = "To")]
        public DateTime To { get; set; }
    }
}