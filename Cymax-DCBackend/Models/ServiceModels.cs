﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Web.Services;

namespace Cymax_DCBackend.Models
{
    [DataContract]
    public class DefaultData {

        [DataMember(IsRequired = false)]
	    public int Id
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
	    public bool Exported
        {
            get;
            set;
        }
	}

    public class ProductData : DefaultData
    {

        [DataMember(IsRequired = false)]
        public string Name
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public string ImageUrl
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public string Category
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public string AttributeGroup
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public string Attribute
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public string Description
        {
            get;
            set;
        }

        public List<AttributeData> AttributeDataList
        {
            get;
            set;
        }
    }

    public class ProductReviewData : DefaultData
    {

        [DataMember(IsRequired = false)]
        public int UserId
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public DateTime Date
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public string MacAddress
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public int ProductId
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public string AttributeGroup
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public string Attribute
        {
            get;
            set;
        }


        // Temporary for display only
        public string Name
        {
            get;
            set;
        }

        public int Added
        {
            get;
            set;
        }

        public int Removed
        {
            get;
            set;
        }
    }

    public class UserData : DefaultData
    {
        [DataMember(IsRequired = false)]
        public string Email
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public string TurkAccount
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public string Password
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public DateTime JoinDate
        {
            get;
            set;
        }

    }

    public class AttributeData : DefaultData
    {

        [DataMember(IsRequired = false)]
        public string Category
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public string AttributeGroup
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public string Attribute
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public String ParentCategory
        {
            get;
            set;
        }

        [DataMember(IsRequired = false)]
        public Boolean AttributeAdded
        {
            get;
            set;
        }

    }

    public class JsonObject
    {
        [DataMember(IsRequired = false)]
        public string result
        {
            get;
            set;
        }
    }
}
