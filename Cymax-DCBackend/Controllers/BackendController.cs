﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data.OleDb;
using Cymax_DCBackend.Models;
using System.Web.Security;

namespace Cymax_DCBackend.Controllers
{
    public class BackendController : Controller
    {

        static string ConnectionString = ConfigurationManager.ConnectionStrings["CymaxConnection"].ConnectionString;
        static int ClientId = int.Parse(ConfigurationManager.AppSettings["ClientID"]);

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Products()
        {
            ViewBag.Title = "Products";

            return View();
        }

        public ActionResult ImportProducts()
        {
            ViewBag.Title = "Import Products";

            if (Session["AuthState"] != "Authenticated")
            {
                return RedirectToAction("Login", "User");
            }
            else
            {
                return View();
            }
        }

        public ActionResult ImportAttributes()
        {
            ViewBag.Title = "Import Attributes";

            if (Session["AuthState"] != "Authenticated")
            {
                return RedirectToAction("Login", "User");
            }
            else
            {
                return View();
            }
        }


        public ActionResult Attributes()
        {
            ViewBag.Title = "Attributes";

            return View();
        }

        public ActionResult dbProductsUpdate(string filePath)
        {

            DataTable sheetData = new DataTable();
            using (OleDbConnection conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR={1}'"))
            {

                //Get the name of First Sheet
                conn.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();

                // retrieve the data using data adapter
                OleDbDataAdapter sheetAdapter = new OleDbDataAdapter("select 1 as ClientID, * from [" + SheetName + "]", conn);
                sheetAdapter.Fill(sheetData);
            }

            string consString = ConfigurationManager.ConnectionStrings["CymaxConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                {
                    //Set the database table name
                    sqlBulkCopy.ColumnMappings.Add("ClientID", "ClientID");
                    sqlBulkCopy.ColumnMappings.Add("Data ID", "ProductID");
                    sqlBulkCopy.ColumnMappings.Add("Product Name", "Name");
                    sqlBulkCopy.ColumnMappings.Add("Category", "Category");
                    sqlBulkCopy.ColumnMappings.Add("Product Description", "Description");
                    sqlBulkCopy.ColumnMappings.Add("Product URL", "Url");
                    sqlBulkCopy.ColumnMappings.Add("Image URL (Main)", "ImageUrl");
                    //sqlBulkCopy.ColumnMappings.Add("Image URL (Extra 1)", "ImageUrl2");
                    sqlBulkCopy.DestinationTableName = "dbo.Products";
                    con.Open();
                    sqlBulkCopy.WriteToServer(sheetData);
                    con.Close();
                }
            }

            return View("Products");
        }

        public ActionResult dbAttributesUpdate(string filePath)
        {
            DataTable sheetData = new DataTable();
            using (OleDbConnection conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR={1}'"))
            {

                //Get the name of First Sheet
                conn.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                conn.Close();

                conn.Open();
                // retrieve the data using data adapter
                OleDbDataAdapter sheetAdapter = new OleDbDataAdapter("select 1 as ClientID, * from [" + SheetName + "]", conn);
                sheetAdapter.Fill(sheetData);
            }

            string consString = ConfigurationManager.ConnectionStrings["CymaxConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                {
                    //Set the database table name
                    sqlBulkCopy.ColumnMappings.Add("ClientID", "ClientID");
                    sqlBulkCopy.ColumnMappings.Add("Category", "Category");
                    sqlBulkCopy.ColumnMappings.Add("Attribute Group", "AttributeGroup");
                    sqlBulkCopy.ColumnMappings.Add("Attribute", "Attribute");
                    sqlBulkCopy.ColumnMappings.Add("Parent Category", "ParentCategory");
                    sqlBulkCopy.DestinationTableName = "Attributes";
                    con.Open();
                    sqlBulkCopy.WriteToServer(sheetData);
                    con.Close();
                }
            }

            return View("Products");
        }

        // file upload for the products
        [HttpPost]
        public ActionResult ImportProducts(HttpPostedFileBase file)
        {
            if (file != null)
            {
                // check if the file is an .xlsx file or an unknow MIME
                if (file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.ContentType == "application/octet-stream")
                {
                    try
                    {
                        string fileName = "products_" + Guid.NewGuid().ToString("N");
                        string path = Path.Combine(Server.MapPath("~/App_Data"), fileName);
                        file.SaveAs(path);
                        ViewBag.Message = "File uploaded successfully";
                        dbProductsUpdate(path);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    }
                }
                else
                    ViewBag.Message = "Only .xlsx files are accepted!";
            }
            else
                ViewBag.Message = "Select a file to upload";

            return View("ImportProducts");
        }

        // file upload for the attributes
        [HttpPost]
        public ActionResult ImportAttributes(HttpPostedFileBase file)
        {
            if (file != null)
            {
                // check if the file is an .xlsx file or an unknown MIME
                if (file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.ContentType == "application/octet-stream")
                {
                    try
                    {
                        string fileName = "attributes_" + Guid.NewGuid().ToString("N");
                        string path = Path.Combine(Server.MapPath("~/App_Data"), fileName);
                        file.SaveAs(path);
                        ViewBag.Message = "File uploaded successfully";
                        dbAttributesUpdate(path);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    }
                }
                else
                    ViewBag.Message = "Only .xlsx files are accepted!";
            }
            else
                ViewBag.Message = "Select a file to upload";

            return View("ImportAttributes");
        }

        public ActionResult ProductsReview(Models.ProductsReviewModels date)
        {
            if (Session["AuthState"] != "Authenticated")
            {
                return RedirectToAction("Login", "User");
            }
            else
            {
                getReviews();
                return View();
            }
        }

        public void getReviews()
        {
            List<ProductReviewData> prodData = new List<ProductReviewData>();

            string queryString = @"	SELECT  PR.[ProductID]
		                                   ,[Name]
		                                   ,[AttributeGroup]
		                                   ,[Attribute]
		                                   ,SUM(IIF([Added]=1, 1,0)) ADDED
		                                   ,SUM(IIF([Added]=0, 1,0)) REMOVED
	                                  FROM  [ProductsReview] PR
	                                  JOIN  [Products] P ON P.ProductID = PR.ProductID
                                     WHERE  PR.[ClientID] = @ClientID
                                  GROUP BY  PR.[ProductID]
		                                   ,P.[Name]
		                                   ,[AttributeGroup]
		                                   ,[Attribute]";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.Parameters.AddWithValue("@ClientID", ClientId);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        prodData.Add(new ProductReviewData
                        {
                            ProductId = reader.GetInt32(0),
                            Name = reader.GetString(1),
                            AttributeGroup = reader.GetString(2),
                            Attribute = reader.GetString(3),
                            Added = reader.GetInt32(4),
                            Removed = reader.GetInt32(5),

                        });
                    }
                }
                else
                {
                    Console.WriteLine("No product attributes found.");
                }

            }
            ViewBag.GroupedProducts = prodData
                .GroupBy(p => new { p.ProductId, p.Name })
                .Select(y => new ProductReviewData { ProductId = y.Key.ProductId, Name = y.Key.Name })
                .ToList<ProductReviewData>();

            ViewBag.ProductReviews = prodData;
            ViewBag.DateFrom = String.Empty;
            ViewBag.DateTo = String.Empty;
        }

        [HttpPost]
        public ActionResult Search(ProductsReviewModels date)
        {
            if (Session["AuthState"].ToString() != "Authenticated")
            {
                return RedirectToAction("Login", "User");
            }

            List<ProductReviewData> prodData = new List<ProductReviewData>();

            string queryString = @"	SELECT  PR.[ProductID]
		                                   ,[Name]
		                                   ,[AttributeGroup]
		                                   ,[Attribute]
		                                   ,SUM(IIF([Added]=1, 1,0)) ADDED
		                                   ,SUM(IIF([Added]=0, 1,0)) REMOVED
	                                  FROM  [ProductsReview] PR
	                                  JOIN  [Products] P ON P.ProductID = PR.ProductID
                                     WHERE  PR.[ClientID] = @ClientID 
                                       AND  CONVERT(varchar(10), [Date], 103) BETWEEN CONVERT(varchar(10), @s, 103) 
                                                                                  AND CONVERT(varchar(10), @e, 103)
                                  GROUP BY  PR.[ProductID]
		                                   ,P.[Name]
		                                   ,[AttributeGroup]
		                                   ,[Attribute]";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = new SqlCommand(queryString, connection))
            {
                command.Parameters.AddWithValue("@ClientID", ClientId);
                command.Parameters.AddWithValue("@s", date.From.ToString("dd/MM/yyyy"));
                command.Parameters.AddWithValue("@e", date.To.ToString("dd/MM/yyyy"));
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        prodData.Add(new ProductReviewData
                        {
                            ProductId = reader.GetInt32(0),
                            Name = reader.GetString(1),
                            AttributeGroup = reader.GetString(2),
                            Attribute = reader.GetString(3),
                            Added = reader.GetInt32(4),
                            Removed = reader.GetInt32(5),

                        });
                    }
                }
                else
                {
                    Console.WriteLine("No product attributes found.");
                }

            }
            ViewBag.GroupedProducts = prodData
                .GroupBy(p => new { p.ProductId, p.Name })
                .Select(y => new ProductReviewData { ProductId = y.Key.ProductId, Name = y.Key.Name })
                .ToList<ProductReviewData>();

            ViewBag.ProductReviews = prodData;
            ViewBag.DateFrom = date.From.ToString("yyyy-MM-dd");
            ViewBag.DateTo = date.To.ToString("yyyy-MM-dd");

            return View("ProductsReview");
        }
    }
}