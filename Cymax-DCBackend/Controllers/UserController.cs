﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Cymax_DCBackend.Controllers
{
    public class UserController : Controller
    {
        // GET: /User/
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Models.BackendModels user)
        {
            if (ModelState.IsValid)
            {
                if (user.IsValid(user.UserName, user.Password) == true)
                {
                    //FormsAuthentication.SetAuthCookie(user.UserName, false);
                    Session["AuthState"] = "Authenticated";
                    return RedirectToAction("Index", "Backend");
                }
                else
                {
                    ModelState.AddModelError("", "Login data is incorrect!");
                }
            }
            return View(user);
        }

        public ActionResult Logout()
        {
            //FormsAuthentication.SignOut();
            Session["AuthState"] = "Not authenticated";
            Session.Remove("AuthState");
            return RedirectToAction("Index", "Backend");
        }
    }
}