USE [Cymax-DCBackend]
GO

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT

BEGIN TRANSACTION
GO
-- Create Attributes table to hold Categories and Attributes
CREATE TABLE dbo.[Attributes]
	(
	ClientID varchar(100) NOT NULL,
	Category varchar(100) NOT NULL,
	AttributeGroup varchar(100) NOT NULL,
	Attribute varchar(100) NOT NULL,
	ParentCategory varchar(100) NOT NULL
	)  ON [PRIMARY]
GO

-- Create Table Attributes 
ALTER TABLE dbo.[Attributes] ADD CONSTRAINT
	PK_Attributes PRIMARY KEY CLUSTERED 
	(
	ClientID,
	Category,
	AttributeGroup,
	Attribute,
	ParentCategory
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.[Attributes] SET (LOCK_ESCALATION = TABLE)
GO
-- Necessary for bulk import
CREATE UNIQUE NONCLUSTERED INDEX [IX_Attributes] ON [dbo].[Attributes]
(
	[ClientID] ASC,
	[Category] ASC,
	[AttributeGroup] ASC,
	[Attribute] ASC,
	[ParentCategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

GO
CREATE NONCLUSTERED INDEX IX_Attributes_ParentCategory ON dbo.Attributes
	(
	ParentCategory
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_Attributes_Category ON dbo.Attributes
	(
	Category
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.Attributes SET (LOCK_ESCALATION = TABLE)
GO

-- Create table for the client users
CREATE TABLE [dbo].[Users](
	[ClientID] [int] NOT NULL,
	[UserID] [int] IDENTITY(1,1) NOT NULL UNIQUE,
	[Email] [varchar](150) NULL,
	[TurkAccount] [varchar](150) NULL,
	[Password] [varchar](150) NULL,
	[JoinDate] [datetime] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-- Create tables products
CREATE TABLE [dbo].[Products](
	[ClientID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Name] [varchar](300) NULL,
	[Category] [varchar](300) NULL,
	[Description] [varchar](5000) NULL,
	[Url] [varchar](300) NULL,
	[ImageUrl] [varchar](300) NULL,
	[ImageUrl2] [varchar](300) NULL,
	[ImageUrl3] [varchar](300) NULL
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[ClientID] ASC,
	[ProductID] ASC

)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-- Necessary for bulk import
CREATE UNIQUE NONCLUSTERED INDEX [IX_Products] ON [dbo].[Products]
(
	[ClientID] ASC,
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

GO
CREATE NONCLUSTERED INDEX IX_Products_Category ON dbo.Products
	(
	Category
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.Products SET (LOCK_ESCALATION = TABLE)
GO
-- Create ProductsReview, store information reviwed by users 
CREATE TABLE [dbo].[ProductsReview](
	[ClientID] [int] NOT NULL,
	[ProductsReviewID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[Date] [DateTime] NULL,
	[MacAddress] [varchar](300) NULL,
	[ProductID] [int] NOT NULL,
	[AttributeGroup] [varchar](300) NULL,
	[Attribute] [varchar](300) NULL,
	[Added] [bit] 
 CONSTRAINT [PK_ProductsReview] PRIMARY KEY CLUSTERED 
(
	[ClientID] ASC,
	[ProductsReviewID] ASC

)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


-- Create Table client for multitenancy 
CREATE TABLE [dbo].[Clients](
	[ClientID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[ServerAddress] [varchar](150) NULL,
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[ClientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO [dbo].[Clients]
           ( [Name]
            ,[ServerAddress])
     VALUES
           (  'Cymax'
             ,'')
GO

USE [Cymax-DCBackend]
GO

/****** Object:  Table [dbo].[BackendUsers]    Script Date: 5/22/2016 6:52:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BackendUsers](
	[username] [varchar](150) NOT NULL,
	[password] [varchar](max) NOT NULL,
	[userID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

INSERT INTO [dbo].[BackendUsers]
           ([username]
           ,[password])
     VALUES
           ('admin'
           ,'epWB5TmyaIPOXizTZW5PxQ==')
GO

SET ANSI_PADDING OFF
GO

COMMIT
GO
