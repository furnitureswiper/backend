﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{
    class AuthenticationOperationInvoker : IOperationInvoker
    {
        private IOperationInvoker defaultInvoker;

        public AuthenticationOperationInvoker(IOperationInvoker defaultInvoker)
        {
            this.defaultInvoker = defaultInvoker;
        }

        public object[] AllocateInputs()
        {
            return defaultInvoker.AllocateInputs();
        }

        public object Invoke(object instance, object[] inputs, out object[] outputs)
        {
            string token = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];
            string date = WebOperationContext.Current.IncomingRequest.Headers["Date"];

            if (string.IsNullOrEmpty(token) || string.IsNullOrEmpty(date) || !token.Equals(Helpers.Encryption.GenerateRequestToken(date)))
            {
                outputs = null;
                return "NotAuthorized";
            }
            else
            {
                return defaultInvoker.Invoke(instance, inputs, out outputs);
            }
        }

        public IAsyncResult InvokeBegin(object instance, object[] inputs, AsyncCallback callback, object state)
        {
            return defaultInvoker.InvokeBegin(instance, inputs, callback, state);
        }

        public object InvokeEnd(object instance, out object[] outputs, IAsyncResult result)
        {
            return defaultInvoker.InvokeEnd(instance, out outputs, result);
        }

        public bool IsSynchronous
        {
            get { return defaultInvoker.IsSynchronous; }
        }
    }

    public class AuthorizedMethodAttribute : Attribute, IOperationBehavior
    {
        public void AddBindingParameters(OperationDescription operationDescription, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        { }

        public void ApplyClientBehavior(OperationDescription operationDescription, System.ServiceModel.Dispatcher.ClientOperation clientOperation)
        { }

        public void ApplyDispatchBehavior(OperationDescription operationDescription, System.ServiceModel.Dispatcher.DispatchOperation dispatchOperation)
        {
            IOperationInvoker defaultInvoker = dispatchOperation.Invoker;
            dispatchOperation.Invoker = new AuthenticationOperationInvoker(defaultInvoker);
        }

        public void Validate(OperationDescription operationDescription)
        {}
    }
}
