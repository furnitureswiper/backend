﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{
    public class Encryption
    {

        private static string FIXED_SALT = "CymaxStoresInc2016";
        private static string IV = "Cym@x$tore$Inc16";
                                    
        public static string Encode(string value)
        {
            var hash = System.Security.Cryptography.SHA1.Create();
            var encoder = new System.Text.ASCIIEncoding();
            var combined = encoder.GetBytes(value ?? "");
            return BitConverter.ToString(hash.ComputeHash(combined)).ToLower().Replace("-", "");
        }

        public static string Encrypt(string raw)
        {
            using (var csp = new AesCryptoServiceProvider())
            {
                ICryptoTransform e = GetCryptoTransform(csp, true, raw);
                byte[] inputBuffer = Encoding.UTF8.GetBytes(raw);
                byte[] output = e.TransformFinalBlock(inputBuffer, 0, inputBuffer.Length);

                return Convert.ToBase64String(output).Replace("/",String.Empty);
            }
        }

        public static string GenerateRequestToken(string date)
        {
            return Encrypt(Encrypt(FIXED_SALT + date) + date);
        }


        private static ICryptoTransform GetCryptoTransform(AesCryptoServiceProvider csp, bool encrypting, string password)
        {
            csp.Mode = CipherMode.CBC;
            csp.Padding = PaddingMode.PKCS7;

            var spec = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(password), Encoding.UTF8.GetBytes(FIXED_SALT), 2534);
            byte[] key = spec.GetBytes(16);

            csp.IV = Encoding.UTF8.GetBytes(IV);
            csp.Key = key;
            if (encrypting)
            {
                return csp.CreateEncryptor();
            }
            return csp.CreateDecryptor();
        }

    }
}